"""Main file of the library

This module provides access to the CLI tool and the API

Author:
    Martin Schorfmann
Since:
    2020-12-20
Version:
    2020-12-20
"""

from pathlib import Path
from typing import Text, Union

from fire import Fire
import yaml

from impress_ssg.model import ImpressPresentation

def impress_ssg(path: Union[Text, Path] = Path.cwd()):
    """Impress SSG CLI"""
    if not isinstance(path, Path):
        path = Path(path)

    presentation_data = yaml.safe_load(
        (path / "presentation.yaml").read_text()
    )

    slides_paths = sorted(
        list(
            path.glob(
                presentation_data["slides_glob_path"]
            )
        ),
        key=lambda value: value.as_posix()
    )

    slides_data = [
        yaml.safe_load(
            slide_data_path.read_text()
        )
        for slide_data_path in slides_paths
    ]

    presentation = ImpressPresentation(
        data=presentation_data,
        slides_data=slides_data
    )

    (path / presentation_data["build"] / "index.html").write_text(
        str(presentation.__html__())
    )

if __name__ == "__main__":
    Fire(impress_ssg)
