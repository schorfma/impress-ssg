"""Data Models

Definition of Data Models used to
represent the presentation and its parts.

Author:
    Martin Schorfmann
Since:
    2020-12-20
Version:
    2021-02-13
"""

from abc import abstractmethod
from collections import UserDict, UserList
from typing import Any, Dict, List, Text

from jinja2 import Environment, FileSystemLoader, Template
from markdown import markdown
from markupsafe import Markup

from impress_ssg.transformation import PositionAndSize


class Renderable:

    @property
    @abstractmethod
    def html(self) -> Markup:
        pass

    def __html__(self) -> Markup:
        return self.html

class ImpressComponent(UserDict, Renderable):

    def __init__(
            self,
            data: Dict[Text, Any],
            parent_data: Dict[Text, Any],
            template_environment: Environment
    ):
        super().__init__()

        self.update(
            {
                **parent_data,
                **data
            }
        )

        self.template_environment = template_environment

    @property
    def html(self) -> Markup:
        if "template" in self:
            if isinstance(self["template"], bool) and self["template"]:
                template = Template(self["content"])
            elif isinstance(self["template"], str):
                template = self.template_environment.get_template(
                    self["template"]
                )

            if template:
                if self.get("markdown", True) and "content" in self:
                    self["content"] = markdown(
                        self["content"],
                        extensions=["extra"]
                    )

                return Markup(
                    template.render(**self.data)
                )

        if self.get("markdown", True) and "content" in self:
            return Markup(
                markdown(
                    self["content"],
                    extensions=["extra"]
                )
            )
        elif self.get("html", False) and "content" in self:
            return Markup(
                self["content"]
            )
        else:
            raise ValueError

    def __html__(self) -> Markup:
        return self.html


class ImpressSlide(ImpressComponent):

    def __init__(
            self,
            data: Dict[Text, Any],
            parent_data: Dict[Text, Any],
            positions_sizes: Dict[Text, PositionAndSize],
            template_environment: Environment
    ):
        super().__init__(
            data=data,
            parent_data=parent_data,
            template_environment=template_environment
        )

        self.position_size = PositionAndSize(
            self["position"],
            self["size"],
            self["default_size"],
            key=self["key"],
            positions_sizes=positions_sizes
        )

        self["position"]["x"] = self.position_size.x
        self["position"]["y"] = self.position_size.y
        self["size"]["width"] = self.position_size.width
        self["size"]["height"] = self.position_size.height


class ImpressPresentation(ImpressComponent):

    def __init__(
            self,
            data: Dict[Text, Any],
            slides_data: List[Dict[Text, Any]]
    ):
        super().__init__(
            data={},
            parent_data=data,
            template_environment=Environment(
                loader=FileSystemLoader(
                    data.get(
                        "templates",
                        "templates"
                    )
                )
            )
        )

        self.positions_sizes: Dict[Text, PositionAndSize] = dict()

        self.slides = [
            ImpressSlide(
                data=slide_data,
                parent_data=data,
                positions_sizes=self.positions_sizes,
                template_environment=self.template_environment
            )
            for slide_data in slides_data
        ]

        self["slides"] = self.slides

        self["template"] = self["presentation_template"]
