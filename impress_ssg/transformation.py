"""Classes for Transformations

Classes for handling trasnformations like position or size.

Author:
    Martin Schorfmann
Since:
    2020-02-13
Version:
    2021-02-13
"""

from collections import UserDict
from typing import Any, Dict, Text

class PositionAndSize(UserDict):

    def __init__(
            self,
            position_data: Dict[Text, Any],
            size_data: Dict[Text, Any],
            default_size: Dict[Text, Any],
            key: Text,
            positions_sizes: Dict[Text, "PositionAndSize"]
    ):
        super().__init__()

        self.update(
            {
                **position_data,
                **size_data
            }
        )

        self.default_size = default_size

        self.update(
            self.determine_position(positions_sizes=positions_sizes)
        )

        positions_sizes[key] = self

    def determine_position(
            self,
            positions_sizes: Dict[Text, "PositionAndSize"]
    ) -> Dict[Text, int]:
        position: Dict[Text, int] = dict()

        if "x" in self:
            position["x"] = self["x"]
        elif "x-of" in self:
            x_of = self["x-of"]
            other_position_size = positions_sizes[x_of]
            position["x"] = other_position_size.x
        elif "left-of" in self:
            left_of = self["left-of"]
            other_position_size = positions_sizes[left_of]
            left_bound = other_position_size.left_of(
                padding=self.get(
                    "padding",
                    self.default_size["padding"]
                )
            )
            position["x"] = left_bound - self.half_width

        elif "right-of" in self:
            right_of = self["right-of"]
            other_position_size = positions_sizes[right_of]
            right_bound = other_position_size.right_of(
                padding=self.get(
                    "padding",
                    self.default_size["padding"]
                )
            )
            position["x"] = right_bound + self.half_width
        else:
            position["x"] = 0

        if "y" in self:
            position["y"] = self["y"]
        elif "y-of" in self:
            y_of = self["y-of"]
            other_position_size = positions_sizes[y_of]
            position["y"] = other_position_size.y
        elif "top-of" in self:
            top_of = self["top-of"]
            other_position_size = positions_sizes[top_of]
            top_bound = other_position_size.top_of(
                padding=self.get(
                    "padding",
                    self.default_size["padding"]
                )
            )
            position["y"] = top_bound - self.half_height

        elif "bottom-of" in self:
            bottom_of = self["bottom-of"]
            other_position_size = positions_sizes[bottom_of]
            bottom_bound = other_position_size.bottom_of(
                padding=self.get(
                    "padding",
                    self.default_size["padding"]
                )
            )
            position["y"] = bottom_bound + self.half_height
        else:
            position["y"] = 0

        return position

    @property
    def x(self) -> int:
        return self["x"]

    @property
    def y(self) -> int:
        return self["y"]

    @property
    def width(self) -> int:
        return self.get(
            "width",
            self.default_size["width"]
        ) * self.get("scale", 1)

    @property
    def height(self) -> int:
        return self.get(
            "height",
            self.default_size["height"]
        ) * self.get("scale", 1)

    @property
    def half_width(self) -> int:
        return round(self.width / 2)

    @property
    def half_height(self) -> int:
        return round(self.height / 2)

    def left_of(self, padding: int = 0) -> int:
        return self.x - self.half_width - padding

    def right_of(self, padding: int = 0) -> int:
        return self.x + self.half_width + padding

    def top_of(self, padding: int = 0) -> int:
        return self.y - self.half_height - padding

    def bottom_of(self, padding: int = 0) -> int:
        return self.y + self.half_height + padding
